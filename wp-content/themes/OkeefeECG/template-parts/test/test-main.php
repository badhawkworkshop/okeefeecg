<section class="test-main">
   
  <div class="ecg-container">
    <div class="ecg-display">
      <?php echo file_get_contents_curl(get_template_directory_uri() . "/src/img/ECG_2.svg"); ?>
      <div class="calipers">
        <div class="line first draggable"><span></span></div>
        <div class="line second draggable"><span></span></div>
      </div>
    </div>
  </div>

</section>