

<?php 
//get answer key
$post_id = $template_args[0];
?>

<section class="test-score-sheet test-score-sheet-show" id="score-sheet-id" data-post=<?= $post_id; ?>>

  <div class="answers-title">
    <h2>Answers</h2>
    <div class="expand-collapse">Expand/Collapse All</div>
  </div>

  <div class="answers-container">

    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>General Characteristics</h3>

      <span class="answer-block">
        <label class="checkwrap" for="01_normal">
          <p>Normal ECG</p>
          <?php 
            $file = get_field('01_normal_pdf', 'option', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="01_normal" value="01_normal" id="01_normal"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="02_borderline_normal_normal_variant_ecg">
          <p>Borderline normal/normal variant ECG</p>
          <?php 
            $file = get_field('02_borderline_normal_normal_variant_ecg_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
        <input type="checkbox" name="02_borderline_normal_normal_variant_ecg" value="02_borderline_normal_normal_variant_ecg" id="02_borderline_normal_normal_variant_ecg"><span class="checkmark"></span>
      </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="03_incorrect_electrode_placement">
          <p>Incorrect electrode placement</p>
          <?php 
            $file = get_field('03_incorrect_electrode_placement_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="03_incorrect_electrode_placement" value="03_incorrect_electrode_placement" id="03_incorrect_electrode_placement"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="04_artifact">
          <p>Artifact</p>
          <?php 
            $file = get_field('04_artifact_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="04_artifact" value="04_artifact" id="04_artifact"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Atrial Enlargement</h3>

      <span class="answer-block">
        <label class="checkwrap" for="05_right_atrial_enlargement">
          <p>Right atrial abnormality</p>
          <?php 
            $file = get_field('05_right_atrial_enlargement_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
        <input type="checkbox" name="05_right_atrial_enlargement" value="05_right_atrial_enlargement" id="05_right_atrial_enlargement"><span class="checkmark"></span>
      </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="06_left_atrial_enlargement">
          <p>Left atrial abnormality</p>
          <?php 
            $file = get_field('06_left_atrial_enlargement_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="06_left_atrial_enlargement" value="06_left_atrial_enlargement" id="06_left_atrial_enlargement"><span class="checkmark"></span>
        </label>
      </span>

    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Atrial Rhythms</h3>

      <span class="answer-block">
        <label class="checkwrap" for="07_sinus_rhythm"><p>Sinus rhythm</p>
          <?php 
            $file = get_field('07_sinus_rhythm_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="07_sinus_rhythm" value="07_sinus_rhythm" id="07_sinus_rhythm"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="08_sinus_arrhythmia"><p>Sinus arrhythmia</p>
          <?php 
            $file = get_field('08_sinus_arrhythmia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="08_sinus_arrhythmia" value="08_sinus_arrhythmia" id="08_sinus_arrhythmia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="09_sinus_bradycardia"><p>Sinus bradycardia (<60 BMP)</p>
          <?php 
            $file = get_field('09_sinus_bradycardia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="09_sinus_bradycardia" value="09_sinus_bradycardia" id="09_sinus_bradycardia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="10_sinus_tachycardia"><p>Sinus tachycardia (>100 BMP)</p>
          <?php 
            $file = get_field('10_sinus_tachycardia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="10_sinus_tachycardia" value="10_sinus_tachycardia" id="10_sinus_tachycardia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="11_sinus_pause_or_arrest"><p>Sinus pause or arrest</p>
          <?php 
            $file = get_field('11_sinus_pause_or_arrest_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="11_sinus_pause_or_arrest" value="11_sinus_pause_or_arrest" id="11_sinus_pause_or_arrest"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="12_sinoatrial_sa_exit_block"><p>Sinoatrial (SA) exit block</p>
          <?php 
            $file = get_field('12_sinoatrial_sa_exit_block_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="12_sinoatrial_sa_exit_block" value="12_sinoatrial_sa_exit_block" id="12_sinoatrial_sa_exit_block"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="13_atrial_premature_complexes_apc"><p>Atrial premature complex (APCs)</p>
          <?php 
            $file = get_field('13_atrial_premature_complexes_apc_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="13_atrial_premature_complexes_apc" value="13_atrial_premature_complexes_apc" id="13_atrial_premature_complexes_apc"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="14_atrial_tachycardia"><p>Atrial tachycardia</p>
          <?php 
            $file = get_field('14_atrial_tachycardia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="14_atrial_tachycardia" value="14_atrial_tachycardia" id="14_atrial_tachycardia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="15_multifocal_atrial_tachycardia_mat"><p>Multifocal atrial tachycardia (MAT)</p>
          <?php 
            $file = get_field('15_multifocal_atrial_tachycardia_mat_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="15_multifocal_atrial_tachycardia_mat" value="15_multifocal_atrial_tachycardia_mat" id="15_multifocal_atrial_tachycardia_mat"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="16_supraventricular_tachycardia_svt"><p>Supraventricular tachycardia (SVT)</p>
          <?php 
            $file = get_field('16_supraventricular_tachycardia_svt_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="16_supraventricular_tachycardia_svt" value="16_supraventricular_tachycardia_svt" id="16_supraventricular_tachycardia_svt"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="17_atrial_flutter"><p>Atrial flutter</p>
          <?php 
            $file = get_field('17_atrial_flutter_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="17_atrial_flutter" value="17_atrial_flutter" id="17_atrial_flutter"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="18_atrial_fibrillation"><p>Atrial fibrillation (AFIB)</p>
          <?php 
            $file = get_field('18_atrial_fibrillation_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="18_atrial_fibrillation" value="18_atrial_fibrillation" id="18_atrial_fibrillation"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Junctional Rhythms</h3>

      <span class="answer-block">
        <label class="checkwrap" for="19_av_junctional_premature_complexes_jpc"><p>AV junctional premature complex (JPCs)</p>
          <?php 
            $file = get_field('19_av_junctional_premature_complexes_jpc_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="19_av_junctional_premature_complexes_jpc" value="19_av_junctional_premature_complexes_jpc" id="19_av_junctional_premature_complexes_jpc"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="20_av_junctional_escape_complex_es"><p>AV junctional escape complex(es)</p>
          <?php 
            $file = get_field('20_av_junctional_escape_complex_es_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="20_av_junctional_escape_complex_es" value="20_av_junctional_escape_complex_es" id="20_av_junctional_escape_complex_es"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="21_av_junctional_rhythmtachycardia"><p>AV junctional rhythm/tachycardia</p>
          <?php 
            $file = get_field('21_av_junctional_rhythmtachycardia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="21_av_junctional_rhythmtachycardia" value="21_av_junctional_rhythmtachycardia" id="21_av_junctional_rhythmtachycardia"><span class="checkmark"></span>
        </label>
      </span>
    </div>



    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Ventricular Rhythms</h3>

      <span class="answer-block">
        <label class="checkwrap" for="22_ventricular_premature_complexes_vpc"><p>Ventricular premature complex (VPCs)</p>
          <?php 
            $file = get_field('22_ventricular_premature_complexes_vpc_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="22_ventricular_premature_complexes_vpc" value="22_ventricular_premature_complexes_vpc" id="22_ventricular_premature_complexes_vpc"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="23_ventricular_parasystole"><p>Ventricular parasystole</p>
          <?php 
            $file = get_field('23_ventricular_parasystole_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="23_ventricular_parasystole" value="23_ventricular_parasystole" id="23_ventricular_parasystole"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="24_ventricular_tachycardia_3_successive_vpcs_vt"><p>Ventricular tachycardia (≥ 3 successive VPCs) (VT)</p>
          <?php 
            $file = get_field('24_ventricular_tachycardia_3_successive_vpcs_vt_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="24_ventricular_tachycardia_3_successive_vpcs_vt" value="24_ventricular_tachycardia_3_successive_vpcs_vt" id="24_ventricular_tachycardia_3_successive_vpcs_vt"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="25_accelerated_idioventricular_rhythm_aivr"><p>Accelerated idioventricular rhythm (AIVR)</p>
          <?php 
            $file = get_field('25_accelerated_idioventricular_rhythm_aivr_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="25_accelerated_idioventricular_rhythm_aivr" value="25_accelerated_idioventricular_rhythm_aivr" id="25_accelerated_idioventricular_rhythm_aivr"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="26_ventricular_escape_complexesrhythm"><p>Ventricular escape complex(es)/rhythm</p>
          <?php 
            $file = get_field('26_ventricular_escape_complexesrhythm_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="26_ventricular_escape_complexesrhythm" value="26_ventricular_escape_complexesrhythm" id="26_ventricular_escape_complexesrhythm"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="27_ventricular_fibrillation_vf"><p>Ventricular fibrillation (VF)</p>
          <?php 
            $file = get_field('27_ventricular_fibrillation_vf_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="27_ventricular_fibrillation_vf" value="27_ventricular_fibrillation_vf" id="27_ventricular_fibrillation_vf"><span class="checkmark"></span>
        </label>
      </span>
    </div>




    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>AV Node Conduction Abnormalities</h3>

      <span class="answer-block">
        <label class="checkwrap" for="28_av_block_1"><p>AV block, 1°</p>
          <?php 
            $file = get_field('28_av_block_1_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="28_av_block_1" value="28_av_block_1" id="28_av_block_1"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="29_av_block_2_-_mobitz_type_i_wenckebach"><p>AV block, 2° - Mobitz type I (Wenckebach)</p>
          <?php 
            $file = get_field('29_av_block_2_-_mobitz_type_i_wenckebach_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="29_av_block_2_-_mobitz_type_i_wenckebach" value="29_av_block_2_-_mobitz_type_i_wenckebach" id="29_av_block_2_-_mobitz_type_i_wenckebach"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="30_av_block_2_-_mobitz_type_ii"><p>AV block, 2° - Mobitz type II</p>
          <?php 
            $file = get_field('30_av_block_2_-_mobitz_type_ii_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="30_av_block_2_-_mobitz_type_ii" value="30_av_block_2_-_mobitz_type_ii" id="30_av_block_2_-_mobitz_type_ii"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="31_av_block_2-1"><p>AV block 2:1</p>
          <?php 
            $file = get_field('31_av_block_2-1_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="31_av_block_2-1" value="31_av_block_2-1" id="31_av_block_2-1"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="32_av_block_3_complete_heart_block"><p>AV block, 3° (complete heart block)</p>
          <?php 
            $file = get_field('32_av_block_3_complete_heart_block_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="32_av_block_3_complete_heart_block" value="32_av_block_3_complete_heart_block" id="32_av_block_3_complete_heart_block"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="33_wolff-parkinson-white_pattern_wpw"><p>Wolff-Parkinson-White pattern (WPW)</p>
          <?php 
            $file = get_field('33_wolff-parkinson-white_pattern_wpw_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="33_wolff-parkinson-white_pattern_wpw" value="33_wolff-parkinson-white_pattern_wpw" id="33_wolff-parkinson-white_pattern_wpw"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="34_av_dissociation"><p>AV dissociation</p>
          <?php 
            $file = get_field('34_av_dissociation_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="34_av_dissociation" value="34_av_dissociation" id="34_av_dissociation"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>QRS Voltage/Axis Abnormalities</h3>

      <span class="answer-block">
        <label class="checkwrap" for="35_low_voltage_limb_leads"><p>Low voltage, limb leads</p>
          <?php 
            $file = get_field('35_low_voltage_limb_leads_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="35_low_voltage_limb_leads" value="35_low_voltage_limb_leads" id="35_low_voltage_limb_leads"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="36_low_voltage_precordial_leads"><p>Low voltage, precordial leads</p>
          <?php 
            $file = get_field('36_low_voltage_precordial_leads_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="36_low_voltage_precordial_leads" value="36_low_voltage_precordial_leads" id="36_low_voltage_precordial_leads"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="37_left_axis_deviation"><p>Left axis deviation (< - 30&deg;)</p>
          <?php 
            $file = get_field('37_left_axis_deviation_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="37_left_axis_deviation" value="37_left_axis_deviation" id="37_left_axis_deviation"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="37_left_axis_deviation"><p>Right axis deviation (> + 100&deg;)</p>
          <?php 
            $file = get_field('37_left_axis_deviation_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="38_right_axis_deviation" value="38_right_axis_deviation" id="38_right_axis_deviation"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="39_electrical_alternans"><p>Electrical alternans</p>
          <?php 
            $file = get_field('39_electrical_alternans_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="39_electrical_alternans" value="39_electrical_alternans" id="39_electrical_alternans"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Ventricular Hypertrophy</h3>

      <span class="answer-block">
        <label class="checkwrap" for="40_left_ventricular_hypertrophy_lvh"><p>Left ventricular hypertrophy (LVH)</p>
          <?php 
            $file = get_field('40_left_ventricular_hypertrophy_lvh_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="40_left_ventricular_hypertrophy_lvh" value="40_left_ventricular_hypertrophy_lvh" id="40_left_ventricular_hypertrophy_lvh"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="41_right_ventricular_hypertrophy_rvh"><p>Right ventricular hypertrophy (RVH)</p>
          <?php 
            $file = get_field('41_right_ventricular_hypertrophy_rvh_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="41_right_ventricular_hypertrophy_rvh" value="41_right_ventricular_hypertrophy_rvh" id="41_right_ventricular_hypertrophy_rvh"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="42_combined_ventricular_hypertrophy"><p>Combined ventricular hypertrophy</p>
          <?php 
            $file = get_field('42_combined_ventricular_hypertrophy_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="42_combined_ventricular_hypertrophy" value="42_combined_ventricular_hypertrophy" id="42_combined_ventricular_hypertrophy"><span class="checkmark"></span>
        </label>
      </span>
    </div>




    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Clinical Disorders</h3>

      <span class="answer-block">
        <label class="checkwrap" for="70_brugada_syndrome"><p>Brugada syndrome</p>
          <?php 
            $file = get_field('70_brugada_syndrome_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="70_brugada_syndrome" value="70_brugada_syndrome" id="70_brugada_syndrome"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="71_digitalis_toxicity"><p>Digitalis toxicity</p>
          <?php 
            $file = get_field('71_digitalis_toxicity_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="71_digitalis_toxicity" value="71_digitalis_toxicity" id="71_digitalis_toxicity"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="72_torsades_de_pointes"><p>Torsades de Pointes</p>
          <?php 
            $file = get_field('72_torsades_de_pointes_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="72_torsades_de_pointes" value="72_torsades_de_pointes" id="72_torsades_de_pointes"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="73_hyperkalemia"><p>Hyperkalemia</p>
          <?php 
            $file = get_field('73_hyperkalemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="73_hyperkalemia" value="73_hyperkalemia" id="73_hyperkalemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="74_hypokalemia"><p>Hypokalemia</p>
          <?php 
            $file = get_field('74_hypokalemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="74_hypokalemia" value="74_hypokalemia" id="74_hypokalemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="75_hypercalcemia"><p>Hypercalcemia</p>
          <?php 
            $file = get_field('75_hypercalcemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="75_hypercalcemia" value="75_hypercalcemia" id="75_hypercalcemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="76_hypocalcemia"><p>Hypocalcemia</p>
          <?php 
            $file = get_field('76_hypocalcemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="76_hypocalcemia" value="76_hypocalcemia" id="76_hypocalcemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="77_dextrocardia_mirror_image"><p>Dextrocardia, mirror image</p>
          <?php 
            $file = get_field('77_dextrocardia_mirror_image_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="77_dextrocardia_mirror_image" value="77_dextrocardia_mirror_image" id="77_dextrocardia_mirror_image"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="78_acute_cor_pulmonalepulmonary_embolus"><p>Acute cor pulmonale/pulmonary embolus</p>
          <?php 
            $file = get_field('78_acute_cor_pulmonalepulmonary_embolus_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="78_acute_cor_pulmonalepulmonary_embolus" value="78_acute_cor_pulmonalepulmonary_embolus" id="78_acute_cor_pulmonalepulmonary_embolus"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="79_pericardial_effusion"><p>Pericardial effusion</p>
          <?php 
            $file = get_field('79_pericardial_effusion_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="79_pericardial_effusion" value="79_pericardial_effusion" id="79_pericardial_effusion"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="80_acute_pericarditis"><p>Acute pericarditis</p>
          <?php 
            $file = get_field('80_acute_pericarditis_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="80_acute_pericarditis" value="80_acute_pericarditis" id="80_acute_pericarditis"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="81_hypertrophic_cardiomyopathy_hcm"><p>Hypertrophic cardiomyopathy (HCM)</p>
          <?php 
            $file = get_field('81_hypertrophic_cardiomyopathy_hcm_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="81_hypertrophic_cardiomyopathy_hcm" value="81_hypertrophic_cardiomyopathy_hcm" id="81_hypertrophic_cardiomyopathy_hcm"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="82_central_nervous_system_cns_disorder"><p>Central nervous system (CNS) disorder</p>
          <?php 
            $file = get_field('82_central_nervous_system_cns_disorder_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="82_central_nervous_system_cns_disorder" value="82_central_nervous_system_cns_disorder" id="82_central_nervous_system_cns_disorder"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="83_hypothermia"><p>Hypothermia</p>
          <?php 
            $file = get_field('83_hypothermia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="83_hypothermia" value="83_hypothermia" id="83_hypothermia"><span class="checkmark"></span>
        </label>
      </span>
    </div>







    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Intraventricular Conduction Abnormalities</h3>

      <span class="answer-block">
        <label class="checkwrap" for="43_right_bundle_branch_block_complete_rbbb"><p>Right bundle branch block, complete (RBBB)</p>
          <?php 
            $file = get_field('43_right_bundle_branch_block_complete_rbbb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="43_right_bundle_branch_block_complete_rbbb" value="43_right_bundle_branch_block_complete_rbbb" id="43_right_bundle_branch_block_complete_rbbb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="44_right_bundle_branch_block_incomplete_irbbb"><p>Right bundle branch block, incomplete (iRBBB)</p>
          <?php 
            $file = get_field('44_right_bundle_branch_block_incomplete_irbbb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="44_right_bundle_branch_block_incomplete_irbbb" value="44_right_bundle_branch_block_incomplete_irbbb" id="44_right_bundle_branch_block_incomplete_irbbb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="46_left_posterior_fascicular_block_lpfb"><p>Left posterior fascicular block (LPFB)</p>
          <?php 
            $file = get_field('46_left_posterior_fascicular_block_lpfb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="46_left_posterior_fascicular_block_lpfb" value="46_left_posterior_fascicular_block_lpfb" id="46_left_posterior_fascicular_block_lpfb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="left_anterior_fascicular_block_lafb"><p>Left Anterior Fascicular Block (LAFB)</p>
          <?php 
            $file = get_field('left_anterior_fascicular_block_lafb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="left_anterior_fascicular_block_lafb" value="left_anterior_fascicular_block_lafb" id="left_anterior_fascicular_block_lafb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="47_left_bundle_branch_block_complete_lbbb"><p>Left bundle branch block, complete (LBBB)</p>
          <?php 
            $file = get_field('47_left_bundle_branch_block_complete_lbbb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="47_left_bundle_branch_block_complete_lbbb" value="47_left_bundle_branch_block_complete_lbbb" id="47_left_bundle_branch_block_complete_lbbb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="48_left_bundle_branch_block_incomplete_ilbbb"><p>Left bundle branch block, incomplete (iLBBB)</p>
          <?php 
            $file = get_field('48_left_bundle_branch_block_incomplete_ilbbb_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="48_left_bundle_branch_block_incomplete_ilbbb" value="48_left_bundle_branch_block_incomplete_ilbbb" id="48_left_bundle_branch_block_incomplete_ilbbb"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="49_aberrant_conduction_including_rate-related"><p>Aberrant conduction (including rate-related)</p>
          <?php 
            $file = get_field('49_aberrant_conduction_including_rate-related_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="49_aberrant_conduction_including_rate-related" value="49_aberrant_conduction_including_rate-related" id="49_aberrant_conduction_including_rate-related"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="50_nonspecific_intraventricular_conduction_disturbance"><p>Nonspecific intraventricular conduction disturbance</p>
          <?php 
            $file = get_field('50_nonspecific_intraventricular_conduction_disturbance_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="50_nonspecific_intraventricular_conduction_disturbance" value="50_nonspecific_intraventricular_conduction_disturbance" id="50_nonspecific_intraventricular_conduction_disturbance"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Q Wave Myocardial Infarction (Age)</h3>

      <span class="answer-block">
        <label class="checkwrap" for="51_anterolateral_mi_acute_or_recent"><p>Anterolateral MI (acute or recent)</p>
          <?php 
            $file = get_field('51_anterolateral_mi_acute_or_recent_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="51_anterolateral_mi_acute_or_recent" value="51_anterolateral_mi_acute_or_recent" id="51_anterolateral_mi_acute_or_recent"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="52_anterolateral_mi_old_or_indeterminate"><p>Anterolateral MI (old or indeterminate)</p>
          <?php 
            $file = get_field('52_anterolateral_mi_old_or_indeterminate_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="52_anterolateral_mi_old_or_indeterminate" value="52_anterolateral_mi_old_or_indeterminate" id="52_anterolateral_mi_old_or_indeterminate"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="53_anterior_or_anteroseptal_mi_acute_or_recent"><p>Anterior or anteroseptal MI (acute or recent)</p>
          <?php 
            $file = get_field('53_anterior_or_anteroseptal_mi_acute_or_recent_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="53_anterior_or_anteroseptal_mi_acute_or_recent" value="53_anterior_or_anteroseptal_mi_acute_or_recent" id="53_anterior_or_anteroseptal_mi_acute_or_recent"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="54_anterior_or_anteroseptal_mi_old_or_indeterminate"><p>Anterior or anteroseptal MI (old or indeterminate)</p>
          <?php 
            $file = get_field('54_anterior_or_anteroseptal_mi_old_or_indeterminate_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="54_anterior_or_anteroseptal_mi_old_or_indeterminate" value="54_anterior_or_anteroseptal_mi_old_or_indeterminate" id="54_anterior_or_anteroseptal_mi_old_or_indeterminate"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="55_lateral_mi_acute_or_recent"><p>Lateral MI (acute or recent)</p>
          <?php 
            $file = get_field('55_lateral_mi_acute_or_recent_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="55_lateral_mi_acute_or_recent" value="55_lateral_mi_acute_or_recent" id="55_lateral_mi_acute_or_recent"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="lateral_mi_old_or_indeterminate"><p>Lateral MI (old or indeterminate)</p>
          <?php 
            $file = get_field('lateral_mi_old_or_indeterminate_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="lateral_mi_old_or_indeterminate" value="lateral_mi_old_or_indeterminate" id="lateral_mi_old_or_indeterminate"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="57_inferior_mi_acute_or_recent"><p>Inferior MI (acute or recent)</p>
          <?php 
            $file = get_field('57_inferior_mi_acute_or_recent_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="57_inferior_mi_acute_or_recent" value="57_inferior_mi_acute_or_recent" id="57_inferior_mi_acute_or_recent"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="58_inferior_mi_old_or_indeterminate"><p>Inferior MI (old or indeterminate)</p>
          <?php 
            $file = get_field('58_inferior_mi_old_or_indeterminate_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="58_inferior_mi_old_or_indeterminate" value="58_inferior_mi_old_or_indeterminate" id="58_inferior_mi_old_or_indeterminate"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="59_posterior_mi_acute_or_recent"><p>Posterior MI (acute or recent)</p>
          <?php 
            $file = get_field('59_posterior_mi_acute_or_recent_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="59_posterior_mi_acute_or_recent" value="59_posterior_mi_acute_or_recent" id="59_posterior_mi_acute_or_recent"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="60_posterior_mi_old_or_indeterminate"><p>Posterior MI (old or indeterminate)</p>
          <?php 
            $file = get_field('60_posterior_mi_old_or_indeterminate_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="60_posterior_mi_old_or_indeterminate" value="60_posterior_mi_old_or_indeterminate" id="60_posterior_mi_old_or_indeterminate"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Repolarization Abnormalities</h3>

      <span class="answer-block">
        <label class="checkwrap" for="61_early_repolarization_normal_variant"><p>Early repolarization, normal variant</p>
          <?php 
            $file = get_field('61_early_repolarization_normal_variant_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="61_early_repolarization_normal_variant" value="61_early_repolarization_normal_variant" id="61_early_repolarization_normal_variant"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="62_juvenile_t_waves_normal_variant"><p>Juvenile T waves, normal variant</p>
          <?php 
            $file = get_field('62_juvenile_t_waves_normal_variant_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="62_juvenile_t_waves_normal_variant" value="62_juvenile_t_waves_normal_variant" id="62_juvenile_t_waves_normal_variant"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="63_st-t_changes_nonspecific"><p>ST-T changes, nonspecific</p>
          <?php 
            $file = get_field('63_st-t_changes_nonspecific_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="63_st-t_changes_nonspecific" value="63_st-t_changes_nonspecific" id="63_st-t_changes_nonspecific"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="64_st-t_changes_suggesting_myocardial_ischemia"><p>ST-T changes suggesting myocardial ischemia</p>
          <?php 
            $file = get_field('64_st-t_changes_suggesting_myocardial_ischemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="64_st-t_changes_suggesting_myocardial_ischemia" value="64_st-t_changes_suggesting_myocardial_ischemia" id="64_st-t_changes_suggesting_myocardial_ischemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="65_st-t_changes_suggesting_myocardial_injury"><p>ST-T changes suggesting myocardial injury</p>
          <?php 
            $file = get_field('65_st-t_changes_suggesting_myocardial_injury_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="65_st-t_changes_suggesting_myocardial_injury" value="65_st-t_changes_suggesting_myocardial_injury" id="65_st-t_changes_suggesting_myocardial_injury"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="66_st-t_changes_suggesting_electrolyte_disturbance"><p>ST-T changes suggesting electrolyte disturbance</p>
          <?php 
            $file = get_field('66_st-t_changes_suggesting_electrolyte_disturbance_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="66_st-t_changes_suggesting_electrolyte_disturbance" value="66_st-t_changes_suggesting_electrolyte_disturbance" id="66_st-t_changes_suggesting_electrolyte_disturbance"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="67_st-t_changes_of_hypertrophy"><p>ST-T changes of hypertrophy</p>
          <?php 
            $file = get_field('67_st-t_changes_of_hypertrophy_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="67_st-t_changes_of_hypertrophy" value="67_st-t_changes_of_hypertrophy" id="67_st-t_changes_of_hypertrophy"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="68_prolonged_qt_interval"><p>Prolonged QT interval</p>
          <?php 
            $file = get_field('68_prolonged_qt_interval_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="68_prolonged_qt_interval" value="68_prolonged_qt_interval" id="68_prolonged_qt_interval"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="69_prominent_u_waves"><p>Prominent U wave(s)</p>
          <?php 
            $file = get_field('69_prominent_u_waves_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="69_prominent_u_waves" value="69_prominent_u_waves" id="69_prominent_u_waves"><span class="checkmark"></span>
        </label>
      </span>
    </div>


    <!-- <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Clinical Disorders</h3>

      <span class="answer-block">
        <label class="checkwrap" for="70_brugada_syndrome"><p>Brugada syndrome</p>
          <?php 
            $file = get_field('70_brugada_syndrome_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="70_brugada_syndrome" value="70_brugada_syndrome" id="70_brugada_syndrome"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="71_digitalis_toxicity"><p>Digitalis toxicity</p>
          <?php 
            $file = get_field('71_digitalis_toxicity_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="71_digitalis_toxicity" value="71_digitalis_toxicity" id="71_digitalis_toxicity"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="72_torsades_de_pointes"><p>Torsades de Pointes</p>
          <?php 
            $file = get_field('72_torsades_de_pointes_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="72_torsades_de_pointes" value="72_torsades_de_pointes" id="72_torsades_de_pointes"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="73_hyperkalemia"><p>Hyperkalemia</p>
          <?php 
            $file = get_field('73_hyperkalemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="73_hyperkalemia" value="73_hyperkalemia" id="73_hyperkalemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="74_hypokalemia"><p>Hypokalemia</p>
          <?php 
            $file = get_field('74_hypokalemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="74_hypokalemia" value="74_hypokalemia" id="74_hypokalemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="75_hypercalcemia"><p>Hypercalcemia</p>
          <?php 
            $file = get_field('75_hypercalcemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="75_hypercalcemia" value="75_hypercalcemia" id="75_hypercalcemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="76_hypocalcemia"><p>Hypocalcemia</p>
          <?php 
            $file = get_field('76_hypocalcemia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="76_hypocalcemia" value="76_hypocalcemia" id="76_hypocalcemia"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="77_dextrocardia_mirror_image"><p>Dextrocardia, mirror image</p>
          <?php 
            $file = get_field('77_dextrocardia_mirror_image_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="77_dextrocardia_mirror_image" value="77_dextrocardia_mirror_image" id="77_dextrocardia_mirror_image"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="78_acute_cor_pulmonalepulmonary_embolus"><p>Acute cor pulmonale/pulmonary embolus</p>
          <?php 
            $file = get_field('78_acute_cor_pulmonalepulmonary_embolus_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="78_acute_cor_pulmonalepulmonary_embolus" value="78_acute_cor_pulmonalepulmonary_embolus" id="78_acute_cor_pulmonalepulmonary_embolus"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="79_pericardial_effusion"><p>Pericardial effusion</p>
          <?php 
            $file = get_field('79_pericardial_effusion_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="79_pericardial_effusion" value="79_pericardial_effusion" id="79_pericardial_effusion"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="80_acute_pericarditis"><p>Acute pericarditis</p>
          <?php 
            $file = get_field('80_acute_pericarditis_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="80_acute_pericarditis" value="80_acute_pericarditis" id="80_acute_pericarditis"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="81_hypertrophic_cardiomyopathy_hcm"><p>Hypertrophic cardiomyopathy (HCM)</p>
          <?php 
            $file = get_field('81_hypertrophic_cardiomyopathy_hcm_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="81_hypertrophic_cardiomyopathy_hcm" value="81_hypertrophic_cardiomyopathy_hcm" id="81_hypertrophic_cardiomyopathy_hcm"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="82_central_nervous_system_cns_disorder"><p>Central nervous system (CNS) disorder</p>
          <?php 
            $file = get_field('82_central_nervous_system_cns_disorder_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="82_central_nervous_system_cns_disorder" value="82_central_nervous_system_cns_disorder" id="82_central_nervous_system_cns_disorder"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="83_hypothermia"><p>Hypothermia</p>
          <?php 
            $file = get_field('83_hypothermia_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="83_hypothermia" value="83_hypothermia" id="83_hypothermia"><span class="checkmark"></span>
        </label>
      </span>
    </div> -->


    <div class="open-score-block">
      <i class="fas fa-plus-square"></i>
      <i class="fas fa-window-close"></i>
      <h3>Pacemakers/Function</h3>

      <span class="answer-block">
        <label class="checkwrap" for="84_atrial_or_coronary_sinus_pacing"><p>Atrial or coronary sinus pacing</p>
          <?php 
            $file = get_field('84_atrial_or_coronary_sinus_pacing_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="84_atrial_or_coronary_sinus_pacing" value="84_atrial_or_coronary_sinus_pacing" id="84_atrial_or_coronary_sinus_pacing"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="85_ventricular_demand_pacemaker_vvi_normal"><p>Ventricular demand pacemaker (VVI), normal</p>
          <?php 
            $file = get_field('85_ventricular_demand_pacemaker_vvi_normal_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="85_ventricular_demand_pacemaker_vvi_normal" value="85_ventricular_demand_pacemaker_vvi_normal" id="85_ventricular_demand_pacemaker_vvi_normal"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="86_dual-chamber_pacemaker_ddd_normal"><p>Dual-chamber pacemaker (DDD), normal</p>
          <?php 
            $file = get_field('86_dual-chamber_pacemaker_ddd_normal_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="86_dual-chamber_pacemaker_ddd_normal" value="86_dual-chamber_pacemaker_ddd_normal" id="86_dual-chamber_pacemaker_ddd_normal"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="87_pacemaker_malfunction_failure_to_capture"><p>Pacemaker malfunction, failure to capture</p>
          <?php 
            $file = get_field('87_pacemaker_malfunction_failure_to_capture_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="87_pacemaker_malfunction_failure_to_capture" value="87_pacemaker_malfunction_failure_to_capture" id="87_pacemaker_malfunction_failure_to_capture"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="88_pacemaker_malfunction_failure_to_sense"><p>Pacemaker malfunction, failure to sense</p>
          <?php 
            $file = get_field('88_pacemaker_malfunction_failure_to_sense_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="88_pacemaker_malfunction_failure_to_sense" value="88_pacemaker_malfunction_failure_to_sense" id="88_pacemaker_malfunction_failure_to_sense"><span class="checkmark"></span>
        </label>
      </span>

      <span class="answer-block">
        <label class="checkwrap" for="89_biventricular_pacing_cardiac_resynchronization_therapy"><p>Biventricular pacing (cardiac resynchronization therapy)</p>
          <?php 
            $file = get_field('89_biventricular_pacing_cardiac_resynchronization_therapy_pdf', 'option');
            if($file){?>
            <a href="<?= $file ?>" target="_blank"><i class="fas fa-info-circle"></i></a>
          <?php } ?>
          <input type="checkbox" name="89_biventricular_pacing_cardiac_resynchronization_therapy" value="89_biventricular_pacing_cardiac_resynchronization_therapy" id="89_biventricular_pacing_cardiac_resynchronization_therapy"><span class="checkmark"></span>
        </label>
      </span>
    </div>

  </div>
  
</section>