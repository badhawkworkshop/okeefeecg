

<?php $slug = basename(get_permalink()); ?>
<aside class="test-menu">
  <div class="menu-toggle">
    <i class="fas fa-long-arrow-alt-left"></i>
  </div>
  <div class="ecg-toggle">
    <img src="<?= get_template_directory_uri() ?>/src/icons/test-show_ecg.svg" />
    <p>ECG</p>
  </div>
  <?php if ($slug != "details") { ?>
  <div class="score-toggle">
    <img src="<?= get_template_directory_uri() ?>/src/icons/test-show_score_sheet.svg" />
    <p>Score Sheet</p>
  </div>
  <?php } ?>
  <div class="calipers-toggle">
    <img src="<?= get_template_directory_uri() ?>/src/icons/test-calipers-plus.png" />
    <p>Show Calipers</p>
  </div>
  <div class="calipers-reset">
    <img src="<?= get_template_directory_uri() ?>/src/icons/test-calipers-close.png" />
    <p>Hide Calipers</p>
  </div>
  <div class="zoom-in">
    <!-- <img src="<?= get_template_directory_uri() ?>/src/icons/test-zoom_in.svg" /> -->
    <i class="fas fa-search-plus"></i>
    <p>Zoom In</p>
  </div>
  <div class="zoom-out">
    <!-- <img src="<?= get_template_directory_uri() ?>/src/icons/test-zoom_out.svg" /> -->
    <i class="fas fa-search-minus"></i>
    <p>Zoom Out</p>
  </div>

  <?php if ($slug != "details") { ?>
  <div class="submit-score-btn">
    <img src="<?= get_template_directory_uri() ?>/src/icons/test-submit-2.png" />
    <p>Submit</p>
  </div>
<?php } ?>
</aside>