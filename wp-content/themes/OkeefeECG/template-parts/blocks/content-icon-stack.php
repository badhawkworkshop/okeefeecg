<?php
/**
 * Block Name: Icon Stack 
 *
 * This is the template that displays an icon stack
 */


?>
<section class="grid">
    <div class="container">
      <?php
      if( have_rows('block_icon_stack') ):
          while ( have_rows('block_icon_stack') ) : the_row(); ?>
              <div class="item">
                <div class="image"><img src="<?= the_sub_field('icon') ?>" /></div>
                <span>
                  <h3><?= the_sub_field('headline_top') ?></h3>
                  <h4><?= the_sub_field('headline_bottom') ?></h4>
                  <p><?= the_sub_field('copy') ?></p>
                </span>
              </div>
              <hr />
          <?php endwhile;
      endif;
      ?>
    </div>
  </section>