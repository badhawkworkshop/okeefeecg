<?php
/**
 * Block Name: Testimonial Slider
 *
 * This is the template that displays a series of tesimonies
 */

$headline = get_field('block_testimonial_slider_headline');
$sub_headline = get_field('block_testimonial_slider_sub_headline');
?>


<section class="testimonials">
    <div class="container">
      <h1><?= $headline ?></h1>
      <h2><?= $sub_headline ?></h2>

      <div class="slider">
        <?php
      if( have_rows('block_testimonial_slider') ):
          while ( have_rows('block_testimonial_slider') ) : the_row(); ?>
              <div class="slide">
                <div class="image" style="background: url('<?= the_sub_field('image') ?>') center no-repeat; background-size:cover;"></div>
                <h4><?= the_sub_field('name'); ?></h4>
                <h5><?= the_sub_field('location'); ?></h5>
                <p><?= the_sub_field('copy'); ?></p>
              </div>
          <?php endwhile;
      endif;
      ?>
      </div>
    </div>
  </section>