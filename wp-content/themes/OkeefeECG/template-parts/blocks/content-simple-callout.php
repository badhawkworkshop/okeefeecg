<?php
/**
 * Block Name: Simple Callout Block
 *
 * This is the template that displays a simple callout block
 */


?>


<section class="simple-callout">
    <h2>Learn More About</h2>
    <h1>O'Keefe ECG</h1>
    <a href="#"><div class="btn">About Us</button></a>
</section>