<?php
/**
 * Block Name: Callout with image background
 *
 * This is the template that displays a callout gallery
 */

$bg = get_field('block_callout_gallery_background_image');
$bg_color = get_field('block_callout_gallery_background_color');

$headline = get_field('block_callout_gallery_headline');
$sub_headline = get_field('block_callout_gallery_sub_headline');
$button_text = get_field('block_callout_gallery_button_text');

$link = get_field('block_callout_gallery_link');
$link_url = $link['url'];
$link_target = $link['target'] ? $link['target'] : '_self';

$id = 'home-hero-' . $block['id'];
?>


 <section class="callout" id="<?= $id; ?>">
    <div class="overlay" style="background-color:<?= $bg_color ?>"></div>
    <span>
      <h1><?= $headline ?></h1>
      <h2><?= $sub_headline ?></h2>
      <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><div class="btn"><?= $button_text ?></div></a>
    </span>
  </section>

  <style type="text/css">
  #<?php echo $id; ?> {
    background:url(<?= $bg ?>);
    background-size:cover;
  }
</style>