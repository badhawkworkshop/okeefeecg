<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package okeefeecg
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">

	<?php
	/**
 	 * @hooked okeefeecg_post_header() - 10
 	 * @hooked okeefeecg_post_content() - 20
 	 * 
	 */
	do_action( 'okeefeecg_loop_post' );

	?>

</article><!-- #post-## -->
