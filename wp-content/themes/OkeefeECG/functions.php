<?php
/**
 * okeefeecg functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package okeefeecg
 */
require_once get_template_directory() . '/inc/init.php';