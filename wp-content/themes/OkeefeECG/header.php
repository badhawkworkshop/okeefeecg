<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package okeefeecg
*/

?>
<!DOCTYPE html>
<html class="bdhwk">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js"></script>



<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	


	<header class="global-header">

    <div class="top">
      
    </div>


    <?php 
    $slug = basename(get_permalink());
    // $no_navbar = array( 
    //   'test', 
    //   'register',
    //   'logout',
    //   'members',
    //   'password_reset',
    //   'password-reset',
    //   'account',
    //   'user'
    // );

    ?>
    
    <div class="nav">
      <a href="/">
      <div class="logo">
        <?php echo file_get_contents_curl(get_template_directory_uri() . "/src/img/logo-color.svg"); ?>
      </div>
      </a>

      <nav>
        <ul>
        <?php if ( is_user_logged_in() ) { 

          $user = wp_get_current_user();
          $userid = $user->ID;
          $nicename = $user->user_nicename;
          $firstname = $user->first_name;
          $lastname = $user->last_name;

          ?>
            <a href="/test" ><li>Take A Test</li></a>
            <a href="/results" ><li>Your Results</li></a>
            
            <li class="divider">|</li>
            <li class="user-info">
              <a href="/account">
                <i class="fas fa-user"></i>
                <?= " " . $firstname . " " . $lastname ?>
              </a>
              <a href="/logout" class="logout">Log Out</a>
            </li>

          <?php if ( is_user_logged_in() && WC()->cart->get_cart_contents_count() > 0 ) { ?>
              <li class="divider">|</li>
              <a href="/cart">
                <li>
                  <i class="fas fa-shopping-cart"></i>
                  (<?php echo WC()->cart->get_cart_contents_count(); ?>)
                </li>
              </a>
            <?php } ?>
        <?php } else { ?>
          <li class="user-info">
            <a href="/login">Log In</a>
          </li>
        <?php } ?>

        </ul>
      </nav>
    </div>

    
  </header>

  <div id="nav-trigger"></div>
  