<?php
/**
* Details
*
* @package okeefeecg
*/
get_header(); 

//get the question id to pull
$detail_id = $_GET["dt"];
//this exists if the page is coming from a finished test
$is_first = $_GET["fr"];
$title = get_field('question_title', $detail_id);
?>

<?php get_template_part( 'template-parts/test/test-menu'); ?>


<main>

<section class="details-main">
	

<?php 


$user_id = get_current_user_id();
$user_meta_data = json_decode(get_user_meta($user_id, 'test_data', true), true);

//if no questions have be answered
if($user_meta_data == NULL || $detail_id == ""){ ?>


	<div class="no-results"> 
		<p>You have not answered any questions yet.</p>
		<a href="/test"><div class="btn">Take a Test</div></a>
	</div>
<?php } else { ?>

	<?php 
	

	foreach($user_meta_data as $key => $value) { 

		if ($key == $detail_id){ 

			$title = get_field('question_title', $detail_id);
      $text = get_field('question_text', $detail_id);
			$ecg_image = get_field('question_ecg_image', $detail_id);
      $ecg_image_marked = get_field('question_ecg_image_marked', $detail_id);
      // 0 = id
      // 1 = total points
      // 2 = correct*
      // 3 = wrong
      // 4 = date the question was last answered

      $totalPointsAvailable = okeefeecg_get_total_points($key);
      // 0 = total points available
      // 1 = questions with point values
			?>

			<section class="test-score">
        <p>
          <strong>Total Points:</strong>
          <span class="total-points"><?= $value[1]; ?>/<span class="total-possible"><?= $totalPointsAvailable[0]; ?></span>
        </p>

        <p>You have selected <span class="correct"><?= $value[2]; ?></span> correct answers (out of <?= $totalPointsAvailable[1]; ?>) and <span class="wrong"><?= $value[3]; ?> incorrect.</p>

      </section>


      <section class="test-main test-main-show" id="test-main-id">

          <div class="test-title">
            <h2><?= $title; ?></h2>
            <div class="reset-zoom">Reset Zoom</div>
          </div>

          <div class="ecg-container">
            <div class="ecg-display">
              <?php echo file_get_contents_curl($ecg_image_marked); ?>
              <div class="calipers">
                <div class="line first cal-draggable"><span></span></div>
                <div class="line second cal-draggable"><span></span></div>
              </div>
            </div>
          </div>
          <div class="info">
            <p>Caliper Start Time: <span class="cot" data-time=916>916</span> ms</p>
            <p>Caliper Time Difference: <span class="ctd">9868</span> ms</p>
            <p>Caliper End Time: <span class="ctt" data-time=10784>10784</span> ms</p>
          </div>
      </section>


      <section class="test-text">
        <div class="desc"><?= $text ?></div>
        <?php if ($is_first == 'new') { ?>
        <a href="/test"><div class="btn">Take Another Test ECG</div></a>
        <?php } else { ?>
        <a href="/test?rt=<?= $key ?>"><div class="btn">Retake this test ECG</div></a>
        <?php } ?>
      </section>

		<?php } //end if

		
	} //end foreach

	} //end else ?>


</section>

</main>

<?php get_footer(); ?>