<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package okeefeecg
*/

?>
    <footer>

      <div class="pattern"></div>
      <div class="gradient"></div>
      
      <div class="footer-content">
        <div class="logo"></div>
        <div class="links">
          <ul>
            <a href="/"><li>Terms of Use</li></a>
            <a href="/"><li>Privacy Policy</li></a>
          </ul>
          <div class="footer-tag">
            <p>&copy; <?= date("Y") ?> O'Keefe ECG. All Rights Reserved.</p>
          </div>
        </div>
      </div>

    
    </footer>

    </div><!-- #page -->
    <?php wp_footer(); 

    // add Google Anlaytics if we are on the production site
    if ($_SERVER['HTTP_HOST']==="XXXXXXX.com" || $_SERVER['HTTP_HOST']==="www.XXXXXXX.com") { 
        echo "<script>
                window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
                ga('create','UA-XXXXXXX-1','auto');ga('send','pageview')
              </script>
              <script src='https://www.google-analytics.com/analytics.js' async defer></script>";
    } ?>

  </body>
</html>
