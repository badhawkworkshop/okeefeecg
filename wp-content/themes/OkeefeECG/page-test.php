<?php
/**
* Test Page
*
* @package okeefeecg
*/
get_header(); 
?>


<?php get_template_part( 'template-parts/test/test-menu'); ?>

<main>

<?php
  //check to see if this is a retake questions
  $force_retake_id = $_GET["rt"];

  // if there's a forced test value the let's set the args to only pull that question
  if($force_retake_id){
    
    //force the args to pull the correct question
    $question_args = array(
        'post_type' => 'question',
        'posts_per_page' => 1,
        'post__in' => array($force_retake_id)
      );

  // if there is NOT a forced test value the let's randomly pull a question
  } else {

    // get array of question id's that have already been answered
    $excluded_questions = okeefeecg_exclude_finished_questions();
    //grab and display question
    $question_args = array(
        'post_type' => 'question',
        'posts_per_page' => 1,
        'orderby'   => 'rand',
        'post__not_in' => $excluded_questions
      );
  }


  $question_posts = new WP_Query($question_args); 

  if ( $question_posts->have_posts() ){
    while ( $question_posts->have_posts() ): $question_posts->the_post(); 

      $post_id = get_the_ID();
      okeefeecg_display_a_question();
      
    endwhile; 
    wp_reset_postdata();
  } else { ?>

    <section>
        <h1>You've answered all the questions.</h1>
        <br /><br /><br />
        <a href="/results" class="btn">Your Results</a>
    </section>


  <?php wp_reset_postdata(); 
  } ?>

</main>
<?php get_footer(); ?>