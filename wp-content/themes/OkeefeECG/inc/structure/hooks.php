<?php
/**
 * Core Structure Hooks
 *
 * @package okeefeecg
 */


/**
 * General
 * @see  okeefeecg_setup()
 * @see  okeefeecg_scripts()
 */
add_action( 'after_setup_theme',      'okeefeecg_setup' );
add_action( 'wp_enqueue_scripts',     'okeefeecg_scripts',       10 );


//blog
// add_action( 'okeefeecg_loop_post',     'okeefeecg_post_header',     10 );
// add_action( 'okeefeecg_loop_post',     'okeefeecg_post_content',      20 );
