<?php
/**
 * Test and Questions Functionality
 *
 * 
 * @package okeefeecg
 */


if ( ! function_exists( 'okeefeecg_get_answer_scores' ) ) :
  /**
   * gets the answers to the questions and returns an array
   * @since 1.0.0
   */
  function okeefeecg_get_answer_scores($post_id) {

    $answer_key = [];

    $questions = [
      "01_normal",
      "02_borderline_normal_normal_variant_ecg",
      "03_incorrect_electrode_placement",
      "04_artifact",
      "05_right_atrial_enlargement",
      "06_left_atrial_enlargement",
      "07_sinus_rhythm",
      "08_sinus_arrhythmia",
      "09_sinus_bradycardia",
      "10_sinus_tachycardia",
      "11_sinus_pause_or_arrest",
      "12_sinoatrial_sa_exit_block",
      "12_sinoatrial_sa_exit_block",
      "13_atrial_premature_complexes_apc",
      "14_atrial_tachycardia",
      "15_multifocal_atrial_tachycardia_mat",
      "16_supraventricular_tachycardia_svt",
      "17_atrial_flutter",
      "18_atrial_fibrillation",
      "19_av_junctional_premature_complexes_jpc",
      "20_av_junctional_escape_complex_es",
      "21_av_junctional_rhythmtachycardia",
      "22_ventricular_premature_complexes_vpc",
      "23_ventricular_parasystole",
      "24_ventricular_tachycardia_3_successive_vpcs_vt",
      "25_accelerated_idioventricular_rhythm_aivr",
      "26_ventricular_escape_complexesrhythm",
      "27_ventricular_fibrillation_vf",
      "28_av_block_1",
      "29_av_block_2_-_mobitz_type_i_wenckebach",
      "30_av_block_2_-_mobitz_type_ii",
      "31_av_block_2-1",
      "32_av_block_3_complete_heart_block",
      "33_wolff-parkinson-white_pattern_wpw",
      "34_av_dissociation",
      "35_low_voltage_limb_leads",
      "36_low_voltage_precordial_leads",
      "37_left_axis_deviation",
      "38_right_axis_deviation",
      "39_electrical_alternans",
      "40_left_ventricular_hypertrophy_lvh",
      "41_right_ventricular_hypertrophy_rvh",
      "42_combined_ventricular_hypertrophy",
      "70_brugada_syndrome",
      "71_digitalis_toxicity",
      "72_torsades_de_pointes",
      "73_hyperkalemia",
      "74_hypokalemia",
      "75_hypercalcemia",
      "76_hypocalcemia",
      "77_dextrocardia_mirror_image",
      "78_acute_cor_pulmonalepulmonary_embolus",
      "79_pericardial_effusion",
      "80_acute_pericarditis",
      "81_hypertrophic_cardiomyopathy_hcm",
      "82_central_nervous_system_cns_disorder",
      "83_hypothermia",
      "43_right_bundle_branch_block_complete_rbbb",
      "44_right_bundle_branch_block_incomplete_irbbb",
      "46_left_posterior_fascicular_block_lpfb",
      "left_anterior_fascicular_block_lafb",
      "47_left_bundle_branch_block_complete_lbbb",
      "48_left_bundle_branch_block_incomplete_ilbbb",
      "49_aberrant_conduction_including_rate-related",
      "50_nonspecific_intraventricular_conduction_disturbance",
      "51_anterolateral_mi_acute_or_recent",
      "52_anterolateral_mi_old_or_indeterminate",
      "53_anterior_or_anteroseptal_mi_acute_or_recent",
      "54_anterior_or_anteroseptal_mi_old_or_indeterminate",
      "55_lateral_mi_acute_or_recent",
      "lateral_mi_old_or_indeterminate",
      "57_inferior_mi_acute_or_recent",
      "58_inferior_mi_old_or_indeterminate",
      "59_posterior_mi_acute_or_recent",
      "60_posterior_mi_old_or_indeterminate",
      "61_early_repolarization_normal_variant",
      "62_juvenile_t_waves_normal_variant",
      "63_st-t_changes_nonspecific",
      "64_st-t_changes_suggesting_myocardial_ischemia",
      "65_st-t_changes_suggesting_myocardial_injury",
      "66_st-t_changes_suggesting_electrolyte_disturbance",
      "67_st-t_changes_of_hypertrophy",
      "68_prolonged_qt_interval",
      "69_prominent_u_waves",
      "84_atrial_or_coronary_sinus_pacing",
      "85_ventricular_demand_pacemaker_vvi_normal",
      "86_dual-chamber_pacemaker_ddd_normal",
      "87_pacemaker_malfunction_failure_to_capture",
      "88_pacemaker_malfunction_failure_to_sense",
      "89_biventricular_pacing_cardiac_resynchronization_therapy"
    ];


    foreach ($questions as $question){
      $q = get_field($question, $post_id);
      //$points = array("q" => "val")["points"];
      $points = $q['points'];

      // if blank give it a -1 score
      // otherwise make sure the score is an int
      if($points == ""){
        $points = -1;
      } else{
        $points = (int)$points;
      }

      // add to the answer_key array
      array_push($answer_key, $points);
    }

    return $answer_key;
  }
endif;



if ( ! function_exists( 'okeefeecg_get_total_points' ) ) :
  /**
   * gets the total points available for a question
   * @since 1.0.0
   */
  function okeefeecg_get_total_points($post_id) {

    $total = 0;
    $questionsWithPoints = 0;

    $questions = [
      "01_normal",
      "02_borderline_normal_normal_variant_ecg",
      "03_incorrect_electrode_placement",
      "04_artifact",
      "05_right_atrial_enlargement",
      "06_left_atrial_enlargement",
      "07_sinus_rhythm",
      "08_sinus_arrhythmia",
      "09_sinus_bradycardia",
      "10_sinus_tachycardia",
      "11_sinus_pause_or_arrest",
      "12_sinoatrial_sa_exit_block",
      "12_sinoatrial_sa_exit_block",
      "13_atrial_premature_complexes_apc",
      "14_atrial_tachycardia",
      "15_multifocal_atrial_tachycardia_mat",
      "16_supraventricular_tachycardia_svt",
      "17_atrial_flutter",
      "18_atrial_fibrillation",
      "19_av_junctional_premature_complexes_jpc",
      "20_av_junctional_escape_complex_es",
      "21_av_junctional_rhythmtachycardia",
      "22_ventricular_premature_complexes_vpc",
      "23_ventricular_parasystole",
      "24_ventricular_tachycardia_3_successive_vpcs_vt",
      "25_accelerated_idioventricular_rhythm_aivr",
      "26_ventricular_escape_complexesrhythm",
      "27_ventricular_fibrillation_vf",
      "28_av_block_1",
      "29_av_block_2_-_mobitz_type_i_wenckebach",
      "30_av_block_2_-_mobitz_type_ii",
      "31_av_block_2-1",
      "32_av_block_3_complete_heart_block",
      "33_wolff-parkinson-white_pattern_wpw",
      "34_av_dissociation",
      "35_low_voltage_limb_leads",
      "36_low_voltage_precordial_leads",
      "37_left_axis_deviation",
      "38_right_axis_deviation",
      "39_electrical_alternans",
      "40_left_ventricular_hypertrophy_lvh",
      "41_right_ventricular_hypertrophy_rvh",
      "42_combined_ventricular_hypertrophy",
      "70_brugada_syndrome",
      "71_digitalis_toxicity",
      "72_torsades_de_pointes",
      "73_hyperkalemia",
      "74_hypokalemia",
      "75_hypercalcemia",
      "76_hypocalcemia",
      "77_dextrocardia_mirror_image",
      "78_acute_cor_pulmonalepulmonary_embolus",
      "79_pericardial_effusion",
      "80_acute_pericarditis",
      "81_hypertrophic_cardiomyopathy_hcm",
      "82_central_nervous_system_cns_disorder",
      "83_hypothermia",
      "43_right_bundle_branch_block_complete_rbbb",
      "44_right_bundle_branch_block_incomplete_irbbb",
      "46_left_posterior_fascicular_block_lpfb",
      "left_anterior_fascicular_block_lafb",
      "47_left_bundle_branch_block_complete_lbbb",
      "48_left_bundle_branch_block_incomplete_ilbbb",
      "49_aberrant_conduction_including_rate-related",
      "50_nonspecific_intraventricular_conduction_disturbance",
      "51_anterolateral_mi_acute_or_recent",
      "52_anterolateral_mi_old_or_indeterminate",
      "53_anterior_or_anteroseptal_mi_acute_or_recent",
      "54_anterior_or_anteroseptal_mi_old_or_indeterminate",
      "55_lateral_mi_acute_or_recent",
      "lateral_mi_old_or_indeterminate",
      "57_inferior_mi_acute_or_recent",
      "58_inferior_mi_old_or_indeterminate",
      "59_posterior_mi_acute_or_recent",
      "60_posterior_mi_old_or_indeterminate",
      "61_early_repolarization_normal_variant",
      "62_juvenile_t_waves_normal_variant",
      "63_st-t_changes_nonspecific",
      "64_st-t_changes_suggesting_myocardial_ischemia",
      "65_st-t_changes_suggesting_myocardial_injury",
      "66_st-t_changes_suggesting_electrolyte_disturbance",
      "67_st-t_changes_of_hypertrophy",
      "68_prolonged_qt_interval",
      "69_prominent_u_waves",
      "84_atrial_or_coronary_sinus_pacing",
      "85_ventricular_demand_pacemaker_vvi_normal",
      "86_dual-chamber_pacemaker_ddd_normal",
      "87_pacemaker_malfunction_failure_to_capture",
      "88_pacemaker_malfunction_failure_to_sense",
      "89_biventricular_pacing_cardiac_resynchronization_therapy"
    ];

    foreach ($questions as $question){
      $q = get_field($question, $post_id);
      $points = $q['points'];
      $total = $total + (int)$points;

      if($points > 0){
        $questionsWithPoints++;
      }
      
    }

    $totalArray = array($total, $questionsWithPoints);

    return $totalArray;
  }
endif;





if ( ! function_exists( 'okeefeecg_get_test_results' ) ) :
  /**
   * 
   * @since 1.0.0
   */
  function okeefeecg_get_test_results() {
    
    $answers = ( isset( $_GET['answers'] ) ) ? $_GET['answers'] : 1;
    $post_id = ( isset( $_GET['post_id'] ) ) ? $_GET['post_id'] : 1;


    //an array of all questions and the corresponding point value
    $answer_key = okeefeecg_get_answer_scores($post_id);



    // compare answers to answer key
    $counter = 0;
    $point_total = 0;
    $total_possible = 0;
    $amount_correct = 0;
    $amount_wrong = 0;


    foreach ($answer_key as $value){
      //value = the point score for this 

      // add up for total points possible
      if($value > 0){
        $total_possible = $value + $total_possible;
      }
      
      
      $answer = $answers[$counter];
      

      //if they selected this
      if($answer == "true"){
        
        //calc points for selecting this checkbox
        $point_total = $value + $point_total;

        //answered correctly?
        if($value > 0){
          $amount_correct++;
        
        // zero value for a neutral 
        } else if($value == 0) {


        // answered wrong
        } else {
          $amount_wrong++;
        }
        

      // they did not select this
      } else {

        //answered missed?
        if($value > 0){
          $amount_wrong++;
        }

        //any other value doesn't matter


      }

      $counter ++;
    }


    //build object to send over in data
    $send = new stdClass();
    $send->totalPoints = $point_total;
    $send->totalPossible = $total_possible;
    $send->correct = $amount_correct;
    $send->wrong = $amount_wrong;
    $send->questionid = $post_id;

    


    
    // use user metadata to store the score for the user
    // 
    // examples
    // https://stackoverflow.com/questions/27470998/storing-json-value-in-add-post-meta
    // https://www.technouz.com/4416/storing-complex-data-wordpress-custom-field/

    // get user id
    $user_id = get_current_user_id();
    $send->user_id = $user_id;

    //get past user data
    $user_meta_data = get_user_meta($user_id, 'test_data', true);
    //add_user_meta($user_id, 'test_data_dump', $user_meta_data);

    //reset the data to nothing so we can add it back in later as a new item
    delete_user_meta($user_id, 'test_data');

    //decode the user data and store it to use a few lines down
    $new_data = json_decode($user_meta_data, true);
    
    //if nothing is stored let's create a new base set to add on to
    if($user_meta_data == 'null' || !$user_meta_data){
      // below example of what the json looks like in user meta
      // {"396":[396,8,7,6,5],"671":[671,4,3,2,1]}
      // 0 = id
      // 1 = total points
      // 2 = correct
      // 3 = wrong
      // 4 = date the question was last answered
      $user_meta_data = '{"' . $post_id . '":[' . $post_id . ',0,0,0,0]}';
      $new_data = json_decode($user_meta_data, true);
    }

    
    // check if question exists in current user meta data
    $exists_check = okeefeecg_check_question_in_user_meta($new_data, $post_id);


    //if it doesn't let's set a new one
    if(!$exists_check){
      $date = date("Y-m-d H:i:s");

      //add the new data to the end of the object
      //convert the object back into a string
      $holder = json_encode($new_data);
      //remove the last bracket
      $holder = substr($holder,0,-1);
      //add the new data onto the end of the string and close it back up
      $holder = $holder . ',"' . $post_id . '":["' . $post_id . '","' . $point_total . '","' . $amount_correct . '","' . $amount_wrong . '","' . $date . '"]}';
      //convert it back into an object
      $new_data = json_decode($holder);

    // if it's already there let's give it new data
    } else {
      $date = date("Y-m-d H:i:s");
      $new_scores = array($post_id, $point_total, $amount_correct, $amount_wrong, $date);
      $new_data[$post_id] = $new_scores;
    }


    // store question data in user meta
    update_user_meta($user_id, 'test_data', json_encode($new_data, JSON_UNESCAPED_UNICODE));

    // TODO: remove this line the div .additional and the line in test.js
    // we don't need to display the user data anymore
    $send->user_meta_data = json_encode($new_data, JSON_UNESCAPED_UNICODE);

    // send the score data over to the ajax call so that it can be displayed
    $final_data = json_encode($send, JSON_UNESCAPED_UNICODE);


    echo $final_data;


  }
endif;





if ( ! function_exists( 'okeefeecg_check_question_in_user_meta' ) ) :
  /**
   * checks if the question exists in the user meta table
   * needs the post_id and a data object
   * @since 1.0.0
   */
  function okeefeecg_check_question_in_user_meta($meta_data, $post_id) {

    // check if question exists in answers
    $exists_check = false;
    foreach($meta_data as $key => $value) {
      
      if($key == $post_id){
        $exists_check = true;

      } else{
        if($exists_check){
          $exists_check = true;
        } else{ 
          $exists_check = false;
        }
        
      }
    }

    return $exists_check;

  }
endif;



if ( ! function_exists( 'okeefeecg_exclude_finished_questions' ) ) :
  /**
   * returns an array of question ids that have already been answered
   * 
   * @since 1.0.0
   */
  function okeefeecg_exclude_finished_questions() {

    $user_id = get_current_user_id();
    $user_meta_data = get_user_meta($user_id, 'test_data', true);

    // echo "user_id:" . $user_id . "<br /><br />";
    // echo "User Meta Data:" . $user_meta_data . "<br /><br />";

    $new_data = json_decode($user_meta_data, true);

    $finished_questions = array();

    foreach($new_data as $key => $value) {
      array_push($finished_questions, $key);
    }

    return $finished_questions;
    

  }
endif;



if ( ! function_exists( 'okeefeecg_display_a_question' ) ) :
  /**
   * displays the structure used to put a question on the screen
   * 
   * @since 1.0.0
   */
  function okeefeecg_display_a_question() {

      $post_id = get_the_ID();
      $title = get_field('question_title');
      $text = get_field('question_text');
      $ecg_image = get_field('question_ecg_image');
      $ecg_image_marked = get_field('question_ecg_image_marked');
      ?>



        <section class="test-main test-main-show" id="test-main-id">

          <div class="test-title">
            <h2><?= $title; ?></h2>
            <div class="reset-zoom">Reset Zoom</div>
          </div>

          <div class="ecg-container">
            <div class="ecg-display">
              <?php echo file_get_contents_curl($ecg_image); ?>
              <div class="calipers">
                <div class="line first cal-draggable"><span></span></div>
                <div class="line second cal-draggable"><span></span></div>
              </div>
            </div>
          </div>
          <div class="info">
            <p>Caliper Start Time: <span class="cot" data-time=916>916</span> ms</p>
            <p>Caliper Time Difference: <span class="ctd">9868</span> ms</p>
            <p>Caliper End Time: <span class="ctt" data-time=10784>10784</span> ms</p>
          </div>
        </section>


        <section class="test-score">
          <p>
            <strong>Total Points:</strong>
            <span class="total-points"></span>/<span class="total-possible"></span>
          </p>
          <p>You have selected <span class="correct"></span> correct answers (out of “x”) and <span class="wrong"></span> incorrect.</p>
          <?php echo file_get_contents_curl($ecg_image_marked); ?>
          <div class="desc"><?= $text ?></div>
          <a href="/test"><div class="btn" style="margin:35px auto; width:200px; background-color:#ffffff; color:#24438e;">Take Another Test ECG</div></a>
        </section>

        <?php okeefeecg_get_template_part('template-parts/test/test-score-sheet', array($post_id) ) ?>

     <?php 
    
  }
endif;


