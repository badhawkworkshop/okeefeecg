<?php
if ( ! function_exists( 'okeefeecg_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function okeefeecg_setup() {

	add_editor_style( get_template_directory() . '/css/editor-style.css' );


	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );


  add_theme_support( 'woocommerce' );


	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'okeefeecg' ),
		'secondary' => esc_html__( 'Header SubMenu', 'okeefeecg' ),
		'footer' => esc_html__( 'Footer Menu', 'okeefeecg' )
	) );

	/*
	 * Switch default markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );



	/**
	 * Setup custom image sizes
	 */
  //thumb size is 300 x 300
  add_image_size( 'okeefeecg_img_small', 640, 9999, false );
  add_image_size( 'okeefeecg_img_medium', 800, 9999, false );
  add_image_size( 'okeefeecg_img_large', 1000, 9999, false );
	add_image_size( 'okeefeecg_img_x_large', 1500, 9999, false );
	add_image_size( 'okeefeecg_img_full', 2000, 9999, false );
  add_image_size( 'okeefeecg_img_square', 800, 800, true );
	
	
}
endif;


/**
 * Enqueue scripts and styles.
 */
function okeefeecg_scripts() {
	wp_enqueue_style( 'okeefeecg-style', get_stylesheet_uri() );
	wp_enqueue_style( 'okeefeecg-main-style', get_template_directory_uri() . '/css/main.css', '$deps', '1.0.4', 'screen' );


  //JQUERY
  // wp_deregister_script('jquery');
  // wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js", false, null);


  wp_register_script( 'ecg-js', get_template_directory_uri() . '/js/main.min.js', array('jquery'), '1.0.4', true );
  wp_register_script( 'libs', get_template_directory_uri() . '/js/libs/libs.min.js', array('jquery'), '1.0.4', true );
  wp_register_script('slick', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js", false, null);

  //scrollmagic and tweenmagic
  wp_register_script( 'tween-max', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js', array(), '1.20.4', true );
  wp_register_script( 'scrollmagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array(), '2.0.5', true );
  wp_register_script( 'scrollmagic-ani', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', array(), '2.0.5', true );
  wp_register_script( 'add-indicators', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.js', array(), '2.0.5', true );

  //vimeo api
  wp_register_script( 'vimeo', 'https://player.vimeo.com/api/player.js', array(), '1.0.0', true );

  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'ecg-js' );
  wp_enqueue_script('libs');
  wp_enqueue_script('slick');
  wp_enqueue_script('tween-max');
  wp_enqueue_script('scrollmagic');
  wp_enqueue_script('scrollmagic-ani');

  //only use add indicators on a dev server
  if ($_SERVER['HTTP_HOST']==="ecg.test") {
    wp_enqueue_script('add-indicators');
  }


  //any page speciic scripts
  if (is_page('about')) {
    
  }
  

}
add_action( 'wp_enqueue_scripts', 'okeefeecg_scripts' );


// get main.css for the admin Gutenberg blocks
function load_custom_wp_admin_style() {
        wp_register_style( 'okeefeecg-main-style-admin', get_template_directory_uri() . '/css/main.css', false, '1.0.0' );
        wp_enqueue_style( 'okeefeecg-main-style-admin' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );



//setup ajax using ajaxflow
add_action( 'ajaxflow_nopriv_okeefeecg_get_test_results', 'okeefeecg_get_test_results' );
add_action( 'ajaxflow_okeefeecg_get_test_results', 'okeefeecg_get_test_results' );

//setup ajax through admin-ajax
// add_action( 'wp_ajax_nopriv_function_name', 'okeefeecg_function_name' );
// add_action( 'wp_ajax_function_name', 'okeefeecg_load_archive_posts' );




/**
* Allow .svg uploads to media
* 
*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



/**
* This function removes anonymous functions set by a plugin
*/
function remove_anonymous_object_filter( $tag, $class, $method ) {
	$filters = false;

	if ( isset( $GLOBALS['wp_filter'][$tag] ) ) {
		$filters = $GLOBALS['wp_filter'][$tag];
	}

	if ( $filters ) {
		foreach ( $filters as $priority => $filter ) {
			foreach ( $filter as $identifier => $function ) {
				if ( ! is_array( $function ) ) {
					continue;
				}

				if ( ! $function['function'][0] instanceof $class ) {
					continue;
				}

				if ( $method == $function['function'][1] ) {
					remove_filter( $tag, array( $function['function'][0], $method ), $priority );
				}
			}
		}
	}
}
// remove_anonymous_object_filter('woocommerce_before_add_to_cart_button', 'woocommerce_gravityforms', 'woocommerce_gravityform');


/**
* This is how to activate an hourly WP Cron job whenever the site is loaded
* Once it's created it won't do it again unless deleted in WP CRON SCHEDULER
*/
//hook to activate the hourly event
function okeefeecg_projects_cron_activation() {
  if ( !wp_next_scheduled( 'okeefeecg_get_project_data' ) ) {
    wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'okeefeecg_get_project_data');
  }

  if ( !wp_next_scheduled( 'okeefeecg_create_projects' ) ) {
    wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'okeefeecg_create_projects');
  }
}
//add_action('wp', 'okeefeecg_projects_cron_activation');
// run these scripts once an hour 
function okeefeecg_get_project_data(){
  
}


// Add Page Slug to Body Class
// creates a page-[slug] body class
function add_slug_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
  $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

