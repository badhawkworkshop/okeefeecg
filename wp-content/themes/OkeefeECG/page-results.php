<?php
/**
* Results
*
* @package okeefeecg
*/
get_header(); ?>


<main>

<section class="results-main">
<h1>Your Results</h1>
	

<?php 


$user_id = get_current_user_id();
$user_meta_data = json_decode(get_user_meta($user_id, 'test_data', true), true);

//if no questions have be answered
if($user_meta_data == NULL){ ?>


	<div class="no-results"> 
		<p>You have not answered any questions yet.</p>
		<a href="/test"><div class="btn">Take a Test</div></a>
	</div>
<?php } else { ?>

	<?php
	//how many tests the user has taken
	$test_taken = 0;
	// the score total of all test taken
	$total_score = 0;
	// the total points available for all tests taken
	$complete_total_points = 0;
	// the total amount of questions taken
	$total_questions_taken = 0;
	//total questions ansered correct
	$total_questions_correct = 0;


	foreach($user_meta_data as $key => $value) { 

		$test_taken ++;
		$total_score = $total_score + $value[1];
		$total_points_for_question = okeefeecg_get_total_points($key);
		$complete_total_points = $complete_total_points + $total_points_for_question[0];

		//$total_questions_taken = $total_questions_taken + $value[2] + $value[3];
		//$total_questions_correct = $total_questions_correct + $value[2];
	}

	//calc accuracy %
	$accuracy = ($total_score / $complete_total_points)*100;
	?>

	<div class="results-totals">
		<p>Tests Taken <span><?= $test_taken ?></span></p>
		<p>Total Score <span><?= $total_score ?>/<?= $complete_total_points ?></span></p>
		<!-- <p>Correctly Answered <span><?= $total_questions_correct ?>/<?= $total_questions_taken ?></span></p> -->
		<p>Accuracy <span><?= round($accuracy, 1) ?>%</span></p>
	</div>

	


	<?php foreach($user_meta_data as $key => $value) { ?>
	<div class="result"> 
		<?php 
			// $value data map
			// 0 = id
	    // 1 = total points
	    // 2 = correct
	    // 3 = wrong
	    // 4 = date the question was last answered

			//total_points data map
			$total_points = okeefeecg_get_total_points($key);
			// 0 = total points available
      // 1 = questions with point values
		?>
		<h3><?= get_field('question_title', $key); ?></h3>
		<span>
			<p>Score: <strong><?= $value[1] ?>/<?= $total_points[0] ?></strong></p>
			<a href="/details?dt=<?= $key ?>"><div class="btn">Details</div>
			<a href="/test?rt=<?= $key ?>"><div class="btn">Retake</div></a>
		</span>
		<!-- <span>
			<img src="<?= get_field('question_ecg_image', $key); ?>" />
		</span> -->
	</div>
		
<?php } //end for each ?>



<?php } // end else ?>


</section>

</main>

<?php get_footer(); ?>