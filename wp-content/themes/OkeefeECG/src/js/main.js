/*

  dFree Boilerplate

  Site Development by Gigantc
  Developer: Daniel Freeman [@dFree]

*/

document.addEventListener("DOMContentLoaded", function(){
(function ($) {



// WINDOW RESIZE FUNCTIONS -----------------------
var windowWidth = window.outerWidth,
    windowHeight = window.outerHeight;
    
window.addEventListener("resize", function(){
  windowWidth = window.outerWidth;
  windowHeight = window.outerHeight;
});


//returns the direct path to the theme
function getHomeUrl() {
  var href = window.location.hostname;
  if(href == "ecg.test"){
    var index = ('http://' + href + '/wp-content/themes/boiler');
  } else {
    var index = ('https://' + href + '/wp-content/themes/boiler');
  }
  return index;
}



// NAV -----------------------
// init scrollmagic controller and triggers

//don't run on certain pages
// um-page-account um-page-login
if( $('body').hasClass('page-home') ){

  console.log('header ani');
  var controller = new ScrollMagic.Controller();
  var navTrigger = document.getElementById("nav-trigger");

  // tweens that fire when trigger is hit
  // changes the nav state when scrolled
  // this is the starting phase
  var navSwitchTween = TweenMax.from( $('header'), 0.5, {
    backgroundColor: "rgba(255,255,255,0)",
    top: '0px',
    height: '100px',
    borderBottom: '1px solid rgba(225,225,225,0)',
    padding: '0px 0px 25px 0px'
  });
  var navTween = TweenMax.from( $('nav a, nav li'), 0.5, {
    color: '#ffffff'
  });
  var navLogoTween = TweenMax.from( $('header .logo'), 0.5, {
    height: '80px',
    width: '120px'
  });
  var navLogoColorTween = TweenMax.from( $('header .logo svg .st1'), 0.5, {
    fill: '#fff'
  });

  // scrollmagic scene initialization
  var navswitch = new ScrollMagic.Scene({
              duration: 500, 
              triggerElement: navTrigger
            })
            .setTween(navSwitchTween)
            //.addIndicators() // add indicators
            .addTo(controller);

  var nav = new ScrollMagic.Scene({
              duration: 500, 
              triggerElement: navTrigger
            })
            .setTween(navTween)
            .addTo(controller);

  var navlogo = new ScrollMagic.Scene({
              duration: 500, 
              triggerElement: navTrigger
            })
            .setTween(navLogoTween)
            .addTo(controller);
  var navlogo = new ScrollMagic.Scene({
              duration: 500, 
              triggerElement: navTrigger
            })
            .setTween(navLogoColorTween)
            .addTo(controller);

} else {
  console.log('header ani kill');
}



// HOME PAGE GALLERY -----------------------
// slick.js requires jquery

if( $('body').hasClass('home') ){

    $('.hero-callout .slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      autoplay: true,
      autoplaySpeed: 3500,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
    });


    $('.testimonials .slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      dots: true,
      arrows:false,
      autoplay: true,
      autoplaySpeed: 3500,
    });

    // change tag-line navigation
    // $('.gallery').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //   $('.gallery-nav .dot-separated-list .active').toggleClass('active');
    //   $('.gallery-nav .dot-separated-list').find("[data-slide='" + nextSlide + "']").toggleClass('active');
    // });
}




// badhawk tag!!
console.log('--------------------------------------');
console.log('-=   built by badhawkworkshop.com   =-');
console.log('--------------------------------------');



})(jQuery);
});//end