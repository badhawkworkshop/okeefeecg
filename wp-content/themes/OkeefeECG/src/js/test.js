document.addEventListener("DOMContentLoaded", function(){


//globals
var ecgScale = 1;
var caliperScale = 1;


//jquery stuff
(function ($) {


var testPageCheck = $('body').hasClass('page-test');
var detailsPageCheck = $('body').hasClass('page-details');
//console.log("Is Test:" + testPageCheck);

if(testPageCheck || detailsPageCheck){


  //open side menu on test
  $('.menu-toggle').on('click', function(){
    //console.log('slide open menu');
    $('.test-menu').toggleClass('test-menu-open');
  });


  //display calipers
  $('.calipers-toggle').on('click', function(){
    //console.log('show calipers');
    $('.calipers').toggleClass('calipers-display');
    $('.info').toggleClass('info-show');
  });


  //reset calipers
  $('.calipers-reset').on('click', function(){
    //console.log('reset calipers');
    //reset caliper location
    $('.calipers .first, .calipers .second').css({
      'transform': 'translate(0px, 0px)',
    });

    //reset times
    document.querySelector('.cot').textContent = "916";
    document.querySelector('.ctd').textContent = "9868";
    document.querySelector('.ctt').textContent = "10784";

    // reset data
    $('.calipers .first, .calipers .second').attr( "data-x", 0 );
    $('.calipers').removeClass('calipers-display');
    $('.info').removeClass('info-show');
  });


  //open ecg
  $('.ecg-toggle').on('click', function(){
    $('html, body').animate({scrollTop: $("#test-main-id").offset().top-130}, 500);
  });


  //open score sheet
  $('.score-toggle').on('click', function(){
    $('html, body').animate({scrollTop: $("#score-sheet-id").offset().top-130}, 500);
  });


  //open score block
  $('.test-score-sheet .answers-container div i').on('click', function(){
    $(this).parent().toggleClass('open-score-block');
  });

  //expand-collapse all
  var expandState = 'open';
  $('.expand-collapse').on('click', function(){
    
      if(expandState == "closed"){
        $('.test-score-sheet .answers-container div').each(function(){
          $(this).addClass('open-score-block');
        });
        expandState = 'open';


      } else {
        $('.test-score-sheet .answers-container div').each(function(){
          $(this).removeClass('open-score-block');
        });
        expandState = 'closed';
      }
  });




  // ZOOMIES
  //zoom in
  $('.zoom-in').on('click', function(){
    ecgScale = ecgScale + 0.15;
    caliperScale = caliperScale - 0.05;

    //check to make sure it stays below 3
    if(ecgScale > 3){
      ecgScale = 3;
      caliperScale = 0.29999999999999966;
    }

    $('.ecg-display').css({
      transform: 'scale(' + ecgScale + ')'
    });

    $('.ecg-display .calipers .line span').css({
      transform: 'scale(' + (caliperScale) + ')'
    });

  });

  //zoom out
  $('.zoom-out').on('click', function(){
    ecgScale = ecgScale - 0.15;
    caliperScale = caliperScale + 0.05;

    //check to make sure it stays at 1
    if(ecgScale < 1){
      ecgScale = 1;
      caliperScale = 1;
    }


    $('.ecg-display').css({
      transform: 'scale(' + ecgScale + ')'
    });
    $('.ecg-display .calipers .line span').css({
      transform: 'scale(' + (caliperScale) + ')'
    });

  });

  //reset Zoom
  $('.reset-zoom').on('click', function(){
    ecgScale = 1;
    caliperScale = 1;
    $('.ecg-display').css({
      transform: 'scale(' + ecgScale + ')'
    });
    $('.ecg-display .calipers .line span').css({
      transform: 'scale(' + (caliperScale) + ')'
    });
  });


  //submit and check score
  $('.submit-score-btn').on('click', function(){
    
    // holds all answers
    var answers = [];
    var post_id = $('.test-score-sheet').data('post');

    // grab each selected input
    // add them to an array
    $('.test-score-sheet input').each(function(){
      var checked = $(this).is(":checked");
      answers.push(checked);
    });


    //ajax to check
    $.ajax({
      url : '/ajaxflow/okeefeecg_get_test_results',
      type : 'GET',
      data: {
        answers: answers,
        post_id: post_id
      },
      beforeSend : function ( xhr ) {
        //console.log('sending');
      },
      success: function( data ) {

        var finalScores = JSON.parse(data);
        
        // console.log('submitting score success');
        // console.log(finalScores);

        // //lets hide the question
        $(".test-menu, .test-main, .test-score-sheet, .test-title").css({'opacity':'0'});
        // $(".test-score").css({'display':'block'});

        //display scores
        // $('.total-points').text(finalScores.totalPoints);
        // $('.total-possible').text(finalScores.totalPossible);
        // $('.correct').text(finalScores.correct);
        // $('.wrong').text(finalScores.wrong);

        var redirect = window.location.origin + "/details?dt=" + finalScores.questionid + "&fr=new";
        window.location.href = redirect;

      },
      error: function(response){
        //console.log(response);
      }
    });
  });




} // end page check


})(jQuery);


// dragging
// target elements with the "draggable" class
// used for calipers
interact('.cal-draggable')
  .draggable({
    // enable inertial throwing
    inertia: false,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrict({
        restriction: "parent",
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
      }),
    ],
    // enable autoScroll
    autoScroll: true,
    onmove: dragMoveListener,
    onend: function (event) {
    
    }
  });

  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);



    //display the caliper time
    // 291.1 is the number of squares in an ecg
    // each caliper starts off 100px
    var ecgWidth = document.querySelector('.ecg-display').offsetWidth;
    var squarePixels = ecgWidth/291.1;
    var timeFirst = document.querySelector('.cot').getAttribute('data-time');
    var timeSecond = document.querySelector('.ctt').getAttribute('data-time');

    var millisec1 = timeFirst;
    var millisec2 = timeSecond;

    //get time for caliper one
    if (target.classList.contains('first') ){
      timeFirst = (x + 100) / squarePixels;
      var round = Math.round(timeFirst * 10) / 10;
      millisec1 = (round*40);
      var first = document.querySelector('.cot');
      first.textContent = millisec1;
      first.setAttribute('data-time', millisec1);
    }

    //get time for caliper two
    if (target.classList.contains('second') ){
      timeSecond = (x + (ecgWidth - 100) ) / squarePixels;
      var round = Math.round(timeSecond * 10) / 10;
      millisec2 = (round*40);
      var second = document.querySelector('.ctt');
      second.textContent = millisec2;
      second.setAttribute('data-time', millisec2);
    }

    
    //get the new time values
    var diff = (millisec1 > millisec2) ? millisec1 - millisec2 : millisec2 - millisec1;
    // console.log(millisec1 + " : " + millisec2);
    document.querySelector('.ctd').textContent = Math.round(diff * 10) / 10;


  }



  interact('.ecg-display')
  .draggable({
    inertia: false,
    autoScroll: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'ecg-container'
      })
    ],
    onmove: ecgDisplayMoveListener,
    onstart: function (event) {
     // console.log('ecgDisplayMoveListener start');
    },
    onend: function (event) {
     // console.log('ecgDisplayMoveListener done');
    }
  });

  function ecgDisplayMoveListener (event) {

    // console.log('ecgDisplayMoveListener running');

    //only move it if we are zoomed in
    if (ecgScale > 1){

      var target = event.target,
          // keep the dragged position in the data-x/data-y attributes
          x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
          y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform =
      target.style.transform =
        'scale(' + ecgScale + ') translate(' + x + 'px, ' + y + 'px)';

      // update the posiion attributes
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);

    }
  }


});//end


