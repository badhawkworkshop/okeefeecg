<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

//get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );


get_header(); ?>


<main>

<section class="small-hero">
	<span>
	  <h1>O'Keefe ECG Subscriptions</h1>
  </span>
</section>

<section class="about">
  <span>
    <h2>About our subscriptions</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  </span>
</section>



<section class="products">
	<?php 
	$args = array( 
		'post_type' => 'product',
		'posts_per_page' => -1, 
		'orderby' => 'rand' 
	);

  $products = new WP_Query( $args );

  while ( $products->have_posts() ) : $products->the_post(); 
    global $product; 

    //var_dump($product);

    $product_id = $product->get_id();
    $simpleURL =  $product->add_to_cart_url(); 
    ?>

    <div class="custom-product">
    	<?php the_post_thumbnail($product_id, 'thumbnail')?>
    	<h2><?= the_title() ?></h2>
      <p><?php echo $product->get_description(); ?></p>
    	<p class="price">$<?php echo $product->get_price(); ?></p>
      <?php 
      if ( WC()->cart->get_cart_contents_count() == 0 ) { ?>
          <a href="<?= $simpleURL ?>"><div class="btn">Add To Cart</div></a>
      <?php } ?>
    </div>

   <?php 
 	endwhile; 

	?>
</section>


</main>

<?php get_footer(); ?>
